package com.josen.mydemo20201105.controller;

import com.josen.mydemo20201105.annotation.MyDataSource;
import com.josen.mydemo20201105.mapper.PermissionMapper;
import com.josen.mydemo20201105.pojo.Permission;
import com.josen.mydemo20201105.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName PermissionController
 * @Description TODO
 * @Author Josen
 * @Date 2020/11/5 14:41
 **/
@RestController
@RequestMapping("/permission")
public class PermissionController {
    @Autowired
    private PermissionService permissionService;

    // 测试获取默认master-db数据
    @GetMapping("/master")
    public List<Permission> handlerFindAll() {
        // 查询test_master_db数据库中的数据
        List<Permission> list = permissionService.findAll();
        return list;
    }

    // 测试获取指定slave-db数据
    @GetMapping("/slave")
    public List<Permission> handlerFindAll2() {
        List<Permission> list = permissionService.findSlaveAll();
        return list;
    }
}
