package com.josen.mydemo20201105;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class}) // 不加载默认数据源配置
@MapperScan(basePackages = "com.josen.mydemo20201105.mapper")
public class Mydemo20201105Application {
    public static void main(String[] args) {
        SpringApplication.run(Mydemo20201105Application.class, args);
    }
}
