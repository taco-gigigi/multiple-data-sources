package com.josen.mydemo20201105.pojo;

import lombok.Value;

/**
 * @ClassName Permission
 * @Description TODO
 * @Author Josen
 * @Date 2020/11/5 14:18
 **/
@Value
public class Permission {
    private Integer id;
    private String name;
    private String keyword;
    private String description;
    private String type;
    private Integer parent_id;

}
