package com.josen.mydemo20201105.annotation;

import java.lang.annotation.*;

/**
 * 自定义数据源选择注解
 * 在需要切换数据的Dao添加此注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyDataSource {
    String name() default "";
}