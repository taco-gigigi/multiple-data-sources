package com.josen.mydemo20201105.service;

import com.josen.mydemo20201105.annotation.MyDataSource;
import com.josen.mydemo20201105.mapper.PermissionMapper;
import com.josen.mydemo20201105.pojo.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName PermissionService
 * @Description TODO
 * @Author Josen
 * @Date 2020/11/5 15:46
 **/
@Service
public class PermissionService {
    @Autowired
    private PermissionMapper permissionMapper;

    @MyDataSource(name = "slave-db")
    public List<Permission> findSlaveAll(){
        return permissionMapper.findAll();
    }

    public List<Permission> findAll(){
        return permissionMapper.findAll();
    }
}
