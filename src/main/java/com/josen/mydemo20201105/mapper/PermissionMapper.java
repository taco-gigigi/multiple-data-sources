package com.josen.mydemo20201105.mapper;

import com.josen.mydemo20201105.annotation.MyDataSource;
import com.josen.mydemo20201105.pojo.Permission;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @ClassName PermissionMapper
 * @Description TODO
 * @Author Josen
 * @Date 2020/11/5 14:19
 **/
@Repository
public interface PermissionMapper {
    List<Permission> findAll();
}
