package com.josen.mydemo20201105.datasource;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName DynamicDataSourceConfig
 * @Description 引入动态数据源，构建数据源
 * @Author Josen
 * @Date 2020/11/5 14:23
 **/
@Configuration
@Component
public class DynamicDataSourceConfig {
    /**
     * 读取application配置，构建master-db数据源
     */
    @Bean
    @ConfigurationProperties("spring.datasource.druid.master-db")
    public DataSource myMasterDataSource(){
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * 读取application配置，构建slave-db数据源
     */
    @Bean
    @ConfigurationProperties("spring.datasource.druid.slave-db")
    public DataSource  mySlaveDataSource(){
        return DruidDataSourceBuilder.create().build();
    }
    /**
     * 读取application配置，创建动态数据源
     */
    @Bean
    @Primary
    public DynamicDataSource dataSource(DataSource myMasterDataSource, DataSource mySlaveDataSource) {
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put("master-db",myMasterDataSource);
        targetDataSources.put("slave-db", mySlaveDataSource);
        // myMasterDataSource=默认数据源
        // targetDataSources=目标数据源（多个）
        return new DynamicDataSource(myMasterDataSource, targetDataSources);
    }
}
